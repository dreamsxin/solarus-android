#!/bin/sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
host_os=`uname -s | tr "[:upper:]" "[:lower:]"`

SRCDIR=$DIR/src
cd "$SRCDIR"

NDK=$NDK_ROOT
NDKABI=21
NDKVER=$NDK/toolchains/llvm/prebuilt/${host_os}-x86_64/bin
NDKP=$NDKVER/armv7a-linux-androideabi${NDKABI}-
NDKF="--sysroot $NDK/sysroot -I$NDK/sysroot/usr/include/arm-linux-androideabi" # -L$NDK/platforms/android-21/arch-arm/usr/lib"
TARGET_AR="$NDKVER/arm-linux-androideabi-ar rcus 2>/dev/null"
# Android/ARM, armeabi-v7a (ARMv7 VFP), Android 4.0+ (ICS)
NDKARCH="-march=armv7-a -mfloat-abi=softfp" #-Wl,--fix-cortex-a8"
DESTDIR=$DIR/prebuilt/armeabi-v7a
rm "$DESTDIR"/*.a
make clean
make -j4 HOST_CC="gcc -m32" CC="clang" TARGET_AR="$TARGET_AR" BUILDMODE=static CROSS=$NDKP TARGET_SYS=Linux TARGET_FLAGS="$NDKF $NDKARCH"

if [ -f $SRCDIR/src/libluajit.a ]; then
    mv $SRCDIR/src/libluajit.a $DESTDIR/libluajit.a
fi;

# Android/AARCH64, arm64-v8a, Android 4.0+ (ICS)
DESTDIR=$DIR/prebuilt/arm64-v8a
#NDKVER=$NDK/toolchains/aarch64-linux-android-4.9
NDKP=$NDKVER/aarch64-linux-android${NDKABI}-
NDKF="--sysroot $NDK/sysroot -I$NDK/sysroot/usr/include/aarch64-linux-android" # -L$NDK/platforms/android-21/arch-arm64/usr/lib"
TARGET_AR="$NDKVER/aarch64-linux-android-ar rcus 2>/dev/null"
rm "$DESTDIR"/*.a
make clean
make -j4 HOST_CC="gcc" CC="clang" TARGET_AR="$TARGET_AR" BUILDMODE=static CROSS=$NDKP TARGET_SYS=Linux TARGET_FLAGS="$NDKF"

if [ -f $SRCDIR/src/libluajit.a ]; then
    mv $SRCDIR/src/libluajit.a $DESTDIR/libluajit.a
fi;

# Android/x86, x86 (i686 SSE3), Android 4.0+ (ICS)
DESTDIR=$DIR/prebuilt/x86
#NDKVER=$NDK/toolchains/x86-4.9
NDKP=$NDKVER/i686-linux-android${NDKABI}-
NDKF="--sysroot $NDK/sysroot -I$NDK/sysroot/usr/include/i686-linux-android" # -L$NDK/platforms/android-21/arch-x86/usr/lib"
TARGET_AR="$NDKVER/i686-linux-android-ar rcus 2>/dev/null"
rm "$DESTDIR"/*.a
make clean
make -j4 HOST_CC="gcc -m32" CC="clang" TARGET_AR="$TARGET_AR" BUILDMODE=static CROSS=$NDKP TARGET_SYS=Linux TARGET_FLAGS="$NDKF"

if [ -f $SRCDIR/src/libluajit.a ]; then
    mv $SRCDIR/src/libluajit.a $DESTDIR/libluajit.a
fi;

# Android/x86_64, x86_64 (x64 SSE4), Android 4.0+ (ICS)
DESTDIR=$DIR/prebuilt/x86_64
#NDKVER=$NDK/toolchains/x86_64-4.9
NDKP=$NDKVER/x86_64-linux-android${NDKABI}-
NDKF="--sysroot $NDK/sysroot -I$NDK/sysroot/usr/include/x86_64-linux-android" # -L$NDK/platforms/android-21/arch-x86_64/usr/lib"
TARGET_AR="$NDKVER/x86_64-linux-android-ar rcus 2>/dev/null"
rm "$DESTDIR"/*.a
make clean
make -j4 HOST_CC="gcc" CC="clang" TARGET_AR="$TARGET_AR" BUILDMODE=static CROSS=$NDKP TARGET_SYS=Linux TARGET_FLAGS="$NDKF"

if [ -f $SRCDIR/src/libluajit.a ]; then
    mv $SRCDIR/src/libluajit.a $DESTDIR/libluajit.a
fi;

make clean
